import type { Client } from 'discord.js';
import {readdirSync} from 'fs';
import {join} from 'path';
import { Command } from '../types/command';

const loadCommands = async(client:Client) => {
	// loop through all files in the commands folder, check if they are a command. Also check if any were deleted.
	const files = readdirSync(join(__dirname, '..', 'commands'));

	const guilds = client.guilds.cache.toJSON();
    
	for (const guild of guilds) {
		const commands = (await guild.commands.fetch()).toJSON();
        
		// delete commands that are no longer in the folder
		for (const command of commands) {
			if (!files.some(f => f === command.name + '.ts')) {
				await guild.commands.delete(command.id);
				console.log(`Deleted command ${command.name} from ${guild.name}`);
			}
		}

		// create & update commands, also load the command to the client
		for (const command of files) {
			// require the command file so we can access the command object
			// eslint-disable-next-line @typescript-eslint/no-var-requires -- required for this to work
			const cmd = require(join(__dirname, '..', 'commands', command)) as Command;
			// check if the command is in the guild's commands, if not - add it
			if (!commands.some(c => c.name === cmd.name)) {
				await guild.commands.create({
					name: cmd.name,
					description: cmd.description || '-',
					default_permission: cmd.default_permission || false,
					default_member_permissions: cmd.default_member_permissions || 0x0,
					options: cmd.options || [],
					type:1,

				});
				console.log(`Added command ${cmd.name} to ${guild.name}`);
			} else {
				// update the command if it was updated
				const command = commands.find(c => c.name === cmd.name);
				if(!command) continue;
				if (command.description !== cmd.description) {
					await guild.commands.edit(command.id, {
						description: cmd.description || '-',
						default_permission: cmd.default_permission || false,
						default_member_permissions: cmd.default_member_permissions || 0x0,
						options: cmd.options || [],
						type:1,
					});
					console.log(`Updated command ${cmd.name} in ${guild.name}`);
				}
			}

			// load the command to the client
			client.commands.set(cmd.name, cmd);
		}
	}
};

export default loadCommands;