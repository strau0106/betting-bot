import { Client, Interaction } from 'discord.js';

const event = (client: Client, interaction:Interaction) => {
	if(!interaction.isCommand()) return;
	const command = client.commands.get(interaction.commandName);
	if(!command) {
		interaction.reply('Command not found');
		return;
	}
	command.execute(client, interaction);
};

module.exports = event;