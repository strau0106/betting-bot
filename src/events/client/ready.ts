import { Client } from 'discord.js';
import loadCommands from '../../handlers/commands';

const event = async(client:Client) => {
	await loadCommands(client);
	console.log('Logged in as ' + client.user?.tag);
};

module.exports = event;
